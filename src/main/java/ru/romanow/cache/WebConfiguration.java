package ru.romanow.cache;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by romanow on 10.10.16
 */
@Configuration
public class WebConfiguration
        extends WebMvcConfigurerAdapter {}

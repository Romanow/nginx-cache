package ru.romanow.cache.web;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by romanow on 10.10.16
 */
@RestController
@RequestMapping("/rest")
public class ApiController {
    private static final Logger logger = LoggerFactory.getLogger(ApiController.class);

    @GetMapping
    public String ping(HttpServletResponse response) {
        String result = RandomStringUtils.randomAlphanumeric(10);
        logger.info("Response {}", result);
        return result;
    }
}
